function addThis() {
		//alert("this is a test");
		// Create a new element and store it in a variable.
		var newEl = document.createElement('li');
		var nextEl = document.createElement('li');
		
		// Create a text node and store it in a variable.
		var newText = document.createTextNode('Patience');
		var nextText = document.createTextNode('Collaborative');
		newText.className = 'cool';
		nextText.className = 'cool';
		
		
		// Attach the new text node to the new element.
		newEl.appendChild(newText);
		nextEl.appendChild(nextText);
		
		// Find the position where the new element should be added.
		var position = document.getElementsByTagName('ul')[0];
		
		// Insert the new element into its position.
		position.appendChild(newEl);
		position.appendChild(nextEl);
		
		var showTextContent = 'Two more items have been added';    // paragraph to add to the end
		var msg = '<p>' + showTextContent + '</p>';
		var el = document.getElementById('scriptResults');
		el.innerHTML = msg;
	
      }

function hotThis() {
var listItems = document.querySelectorAll('li');                   // All <li> elements

// ADD A CLASS OF COOL TO ALL LIST ITEMS
var i;                                                             // Counter variable
for (i = 0; i < 3; i++) {                           // Loop through elements
  listItems[i].className = 'hot';                                 // Change class to cool
}

var heading = document.querySelector('h2');                        // h2 element
var headingText = heading.firstChild.nodeValue;                    // h2 text
var totalItems = listItems.length;                                 // No. of <li> elements
var newHeading =  headingText + '<h6> Hot Items Highlighted Below </h6>'; // Content
//var newHeading = '<h3> Hot Items Highlited below </h3>';
heading.innerHTML = newHeading;                                    // Update h2 using innerHTML (not textContent) because it contains markup

}

function notReally() {
	var removeEl = document.getElementById('five');
	var containerEl = removeEl.parentNode;
	containerEl.removeChild(removeEl);
	
	var showTextContent = 'A not so important quality has been removed'; 
		var msg = '<p>' + showTextContent + '</p>';
		var el = document.getElementById('scriptResults');
		el.innerHTML = msg;
}



